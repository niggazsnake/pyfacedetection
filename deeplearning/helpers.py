# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import os
import cv2
import sys
from io import BytesIO
from PIL import Image
import datetime
from django.conf import settings
from files2 import DlibAligner
from files2 import FR_CAFFE
import numpy as np
from face.models import Face

sys.path.append('/home/ubuntu/caffe-face/python')


def get_template_from_image(path):
    predictor_path = str(
        os.path.join(settings.BASE_DIR, 'deeplearning', 'files2', 'shape_predictor_68_face_landmarks.dat'))
    dlibAligner = DlibAligner.DlibAligner(predictor_path)
    face_deploy = str(os.path.join(settings.BASE_DIR, 'deeplearning', 'files2', 'face_deploy.prototxt'))
    face_model = str(os.path.join(settings.BASE_DIR, 'deeplearning', 'files2', 'face_model.caffemodel'))
    fr = FR_CAFFE.FR_CAFFE(face_deploy, face_model, 'fc5', False, 0)

    img = cv2.imread(path, 1)
    bb = dlibAligner.getLargestFaceBoundingBox(img, False)

    if bb is not None:
        landmarks = dlibAligner.findLandmarks(img, bb)
        cropped_img = dlibAligner.fitFacePoints(img, landmarks)
        # cv2.imwrite('cropp1.jpg', cropped_img)
        return fr.getTemplate(cropped_img)

    return None


def create_opencv_image_from_stringio(img_stream, cv2_img_flag=0):
    img_stream.seek(0)
    img_array = np.asarray(bytearray(img_stream.read()), dtype=np.uint8)
    return cv2.imdecode(img_array, cv2_img_flag)


def get_template_from_image2(img):
    img = create_opencv_image_from_stringio(img, 1)
    predictor_path = str(
        os.path.join(settings.BASE_DIR, 'deeplearning', 'files2', 'shape_predictor_68_face_landmarks.dat'))
    dlibAligner = DlibAligner.DlibAligner(predictor_path)
    face_deploy = str(os.path.join(settings.BASE_DIR, 'deeplearning', 'files2', 'face_deploy.prototxt'))
    face_model = str(os.path.join(settings.BASE_DIR, 'deeplearning', 'files2', 'face_model.caffemodel'))
    fr = FR_CAFFE.FR_CAFFE(face_deploy, face_model, 'fc5', False, 0)

    # img = cv2.imread(path, 1)
    bb = dlibAligner.getLargestFaceBoundingBox(img, False)

    if bb is not None:
        landmarks = dlibAligner.findLandmarks(img, bb)
        cropped, bb1, landmarks1 = dlibAligner.cropImg(img, bb, landmarks)
        dist = dlibAligner.gey_eye_distance(landmarks1)
        img_size = (96, 112)
        cropped_img = fr.fitFacePoints(cropped, landmarks1, img_size)
        cv2.imwrite('cropp1.jpg', cropped)
        face_template = fr.getTemplate(cropped_img)
        if not is_exist_person(face_template, fr):
            return {'template': face_template, 'distance': dist}
        else:
            return "Такой пользователь уже существует"

    return None


def is_exist_person(template, fr):
    if template is not None and template.any():
        for face in Face.objects.all():
            diff = fr.cosinDistance(template, face.template)
            if diff > 0.7:
                return True
    return False


def path_for_ident_photo():
    y, m, d, h, m, s = datetime.datetime.today().strftime('%Y-%m-%d-%H-%M-%S').split('-')
    path = '/'.join([settings.MEDIA_ROOT, 'ident', y, m, d])
    filename = '_'.join(['ident', h, m, s, '.jpg'])
    if not os.path.exists(path):
        os.makedirs(path)
    file_path = os.path.join(path, filename)
    return file_path


def search_face(img):
    img = create_opencv_image_from_stringio(img, 1)
    predictor_path = str(
        os.path.join(settings.BASE_DIR, 'deeplearning', 'files2', 'shape_predictor_68_face_landmarks.dat'))
    dlibAligner = DlibAligner.DlibAligner(predictor_path)
    face_deploy = str(os.path.join(settings.BASE_DIR, 'deeplearning', 'files2', 'face_deploy.prototxt'))
    face_model = str(os.path.join(settings.BASE_DIR, 'deeplearning', 'files2', 'face_model.caffemodel'))
    fr = FR_CAFFE.FR_CAFFE(face_deploy, face_model, 'fc5', False, 0)
    bb = dlibAligner.getLargestFaceBoundingBox(img, False)
    img_template = None

    if bb is not None:
        landmarks = dlibAligner.findLandmarks(img, bb)
        # cropped_img = dlibAligner.fitFacePoints(img, landmarks)
        cropped, bb1, landmarks1 = dlibAligner.cropImg(img, bb, landmarks)
        img_size = (96, 112)
        cropped_img = fr.fitFacePoints(cropped, landmarks1, img_size)
        path = path_for_ident_photo()
        cv2.imwrite(path, cropped)
        img_template = fr.getTemplate(cropped_img)

        for face in Face.objects.all():
            diff = fr.cosinDistance(img_template, face.template)
            if diff > 0.7:
                return [face, path[path.find(settings.MEDIA_URL):]]

    return None






