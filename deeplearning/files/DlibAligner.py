import sys
import os
import dlib
import glob
import cv2
import numpy as np
import math
import matplotlib.pyplot as plt

TEMPLATE = np.float32([
    (0.0792396913815, 0.339223741112), (0.0829219487236, 0.456955367943),
    (0.0967927109165, 0.575648016728), (0.122141515615, 0.691921601066),
    (0.168687863544, 0.800341263616), (0.239789390707, 0.895732504778),
    (0.325662452515, 0.977068762493), (0.422318282013, 1.04329000149),
    (0.531777802068, 1.06080371126), (0.641296298053, 1.03981924107),
    (0.738105872266, 0.972268833998), (0.824444363295, 0.889624082279),
    (0.894792677532, 0.792494155836), (0.939395486253, 0.681546643421),
    (0.96111933829, 0.562238253072), (0.970579841181, 0.441758925744),
    (0.971193274221, 0.322118743967), (0.163846223133, 0.249151738053),
    (0.21780354657, 0.204255863861), (0.291299351124, 0.192367318323),
    (0.367460241458, 0.203582210627), (0.4392945113, 0.233135599851),
    (0.586445962425, 0.228141644834), (0.660152671635, 0.195923841854),
    (0.737466449096, 0.182360984545), (0.813236546239, 0.192828009114),
    (0.8707571886, 0.235293377042), (0.51534533827, 0.31863546193),
    (0.516221448289, 0.396200446263), (0.517118861835, 0.473797687758),
    (0.51816430343, 0.553157797772), (0.433701156035, 0.604054457668),
    (0.475501237769, 0.62076344024), (0.520712933176, 0.634268222208),
    (0.565874114041, 0.618796581487), (0.607054002672, 0.60157671656),
    (0.252418718401, 0.331052263829), (0.298663015648, 0.302646354002),
    (0.355749724218, 0.303020650651), (0.403718978315, 0.33867711083),
    (0.352507175597, 0.349987615384), (0.296791759886, 0.350478978225),
    (0.631326076346, 0.334136672344), (0.679073381078, 0.29645404267),
    (0.73597236153, 0.294721285802), (0.782865376271, 0.321305281656),
    (0.740312274764, 0.341849376713), (0.68499850091, 0.343734332172),
    (0.353167761422, 0.746189164237), (0.414587777921, 0.719053835073),
    (0.477677654595, 0.706835892494), (0.522732900812, 0.717092275768),
    (0.569832064287, 0.705414478982), (0.635195811927, 0.71565572516),
    (0.69951672331, 0.739419187253), (0.639447159575, 0.805236879972),
    (0.576410514055, 0.835436670169), (0.525398405766, 0.841706377792),
    (0.47641545769, 0.837505914975), (0.41379548902, 0.810045601727),
    (0.380084785646, 0.749979603086), (0.477955996282, 0.74513234612),
    (0.523389793327, 0.748924302636), (0.571057789237, 0.74332894691),
    (0.672409137852, 0.744177032192), (0.572539621444, 0.776609286626),
    (0.5240106503, 0.783370783245), (0.477561227414, 0.778476346951)])

TPL_MIN, TPL_MAX = np.min(TEMPLATE, axis=0), np.max(TEMPLATE, axis=0)
MINMAX_TEMPLATE = (TEMPLATE - TPL_MIN) / (TPL_MAX - TPL_MIN)


class DlibAligner:
    INNER_EYES_AND_BOTTOM_LIP = [39, 42, 57]
    OUTER_EYES_AND_NOSE = [36, 45, 33]
    OUTER_EYE_NOSE_MOUTH = [36, 45, 30, 48, 54]

    def __init__(self, facePredictor):
        """
        Instantiate an 'AlignDlib' object.
        :param facePredictor: The path to dlib's
        :type facePredictor: str
        """
        assert facePredictor is not None

        self.detector = dlib.get_frontal_face_detector()
        self.predictor = dlib.shape_predictor(facePredictor)

    def getAllFaceBoundingBoxes(self, rgbImg):
        """
        Find all face bounding boxes in an image.
        :param rgbImg: RGB image to process. Shape: (height, width, 3)
        :type rgbImg: numpy.ndarray
        :return: All face bounding boxes in an image.
        :rtype: dlib.rectangles
        """
        assert rgbImg is not None

        try:
            return self.detector(rgbImg, 1)
        except Exception as e:
            print("Warning: {}".format(e))
            # In rare cases, exceptions are thrown.
            return []

    def getLargestFaceBoundingBox(self, rgbImg, skipMulti=False):
        """
        Find the largest face bounding box in an image.
        :param rgbImg: RGB image to process. Shape: (height, width, 3)
        :type rgbImg: numpy.ndarray
        :param skipMulti: Skip image if more than one face detected.
        :type skipMulti: bool
        :return: The largest face bounding box in an image, or None.
        :rtype: dlib.rectangle
        """
        assert rgbImg is not None

        faces = self.getAllFaceBoundingBoxes(rgbImg)
        if (not skipMulti and len(faces) > 0) or len(faces) == 1:
            return max(faces, key=lambda rect: rect.width() * rect.height())
        else:
            return None

    def findLandmarks(self, rgbImg, bb):
        """
        Find the landmarks of a face.
        :param rgbImg: RGB image to process. Shape: (height, width, 3)
        :type rgbImg: numpy.ndarray
        :param bb: Bounding box around the face to find landmarks for.
        :type bb: dlib.rectangle
        :return: Detected landmark locations.
        :rtype: list of (x,y) tuples
        """
        assert rgbImg is not None
        assert bb is not None

        points = self.predictor(rgbImg, bb)

        return list(map(lambda p: (p.x, p.y), points.parts()))

    def align(self, imgDim, rgbImg, bb=None,
              landmarks=None, landmarkIndices=INNER_EYES_AND_BOTTOM_LIP,
              skipMulti=False):
        r"""align(imgDim, rgbImg, bb=None, landmarks=None, landmarkIndices=INNER_EYES_AND_BOTTOM_LIP)
        Transform and align a face in an image.
        :param imgDim: The edge length in pixels of the square the image is resized to.
        :type imgDim: int
        :param rgbImg: RGB image to process. Shape: (height, width, 3)
        :type rgbImg: numpy.ndarray
        :param bb: Bounding box around the face to align. \
                   Defaults to the largest face.
        :type bb: dlib.rectangle
        :param landmarks: Detected landmark locations. \
                          Landmarks found on `bb` if not provided.
        :type landmarks: list of (x,y) tuples
        :param landmarkIndices: The indices to transform to.
        :type landmarkIndices: list of ints
        :param skipMulti: Skip image if more than one face detected.
        :type skipMulti: bool
        :return: The aligned RGB image. Shape: (imgDim, imgDim, 3)
        :rtype: numpy.ndarray
        """
        assert imgDim is not None
        assert rgbImg is not None
        assert landmarkIndices is not None

        if bb is None:
            bb = self.getLargestFaceBoundingBox(rgbImg, skipMulti)
            if bb is None:
                return

        if landmarks is None:
            landmarks = self.findLandmarks(rgbImg, bb)

        npLandmarks = np.float32(landmarks)
        npLandmarkIndices = np.array(landmarkIndices)

        H = cv2.getAffineTransform(npLandmarks[npLandmarkIndices],
                                   imgDim * MINMAX_TEMPLATE[npLandmarkIndices])
        thumbnail = cv2.warpAffine(rgbImg, H, (imgDim, imgDim))

        return thumbnail

    def getIndicesLandmark(self, rgbImg, bb=None, landmarks=None, landmarkIndices=OUTER_EYE_NOSE_MOUTH,
                           skipMulti=False):

        assert rgbImg is not None
        assert landmarkIndices is not None

        if bb is None:
            bb = self.getLargestFaceBoundingBox(rgbImg, skipMulti)
            if bb is None:
                return

        if landmarks is None:
            landmarks = self.findLandmarks(rgbImg, bb)

            # npLandmarks = np.float32(landmarks)
        # npLandmarkIndices = np.array(landmarkIndices)

        # points = npLandmarks[npLandmarkIndices]
        # return list(map(lambda p: (p.x, p.y), points.parts()))
        points = []
        for ind in landmarkIndices:
            points.append(landmarks[ind])
        print points
        print points[0][0]
        print points[2][1]
        return points

    # def fitFacePoints(self, img, landmarks):
    # ec_mc_y = 48;  # FOR TRAINING & TEST
    # ec_y = 50  # 40; # FOR TEST
    # # ec_y = 48; # FOR TRAINING
    # # cv::Size img_size(144, 144); # FOR TRAINING
    #     img_size = (96, 112);  #// FOR TEST
    #     rEyeC = ((landmarks[36][0] + landmarks[39][0]) / 2.0, (landmarks[36][1] + landmarks[39][1]) / 2.0);
    #     lEyeC = ((landmarks[42][0] + landmarks[45][0]) / 2.0, (landmarks[42][1] + landmarks[45][1]) / 2.0);
    #     rMouth = (landmarks[48][0], landmarks[48][1]);
    #     lMouth = (landmarks[10][0], landmarks[10][1]);
    #     noseTip = (landmarks[1][0], landmarks[1][1]);
    #
    #     # Rotate according eyes centers
    #     ang_tan = (lEyeC[1] - rEyeC[1]) / (lEyeC[0] - rEyeC[0]);
    #     ang = math.atan(ang_tan) / math.pi * 180.0;
    #     rows, cols, ch = img.shape
    #
    #     r = cv2.getRotationMatrix2D((cols, rows / 2), ang, 1.0);
    #     img_rot = cv2.warpAffine(img, r, (cols, rows));
    #     print r
    #     print r[0, 2]
    #     # Rotate eyes center point
    #     eyesCBase = ((rEyeC[0] + lEyeC[0]) / 2.0, (rEyeC[1] + lEyeC[1]) / 2.0);
    #     eyesC = (r[0, 0] * eyesCBase[0] + r[0, 1] * eyesCBase[1] + r[0, 2],
    #              r[1, 0] * eyesCBase[0] + r[1, 1] * eyesCBase[1] + r[1, 2])
    #
    #     print eyesC
    #     # Rotate mouth center point
    #     mouthCBase = ((rMouth[0] + lMouth[0]) / 2.0, (rMouth[1] + lMouth[1]) / 2.0);
    #     mouthC = (r[0, 0] * mouthCBase[0] + r[0, 1] * mouthCBase[1] + r[0, 2],
    #               r[1, 0] * mouthCBase[0] + r[1, 1] * mouthCBase[1] + r[1, 2]);
    #
    #     # Resize to given ec_mc_y
    #     resize_scale = float(ec_mc_y) / (mouthC[1] - eyesC[1]);
    #     rot_rows, rot_cols, rot_ch = img_rot.shape
    #     img_resized = cv2.resize(img_rot, (int(rot_cols * resize_scale), int(rot_rows * resize_scale)));
    #     eyesC2 = (eyesC[0] * resize_scale, eyesC[1] * resize_scale);
    #     #cv2.imwrite('resized.jpg', img_resized)
    #     #eyesC2[0] = eyesC[0] * resize_scale;#(eyesC.x - img_rot.cols / 2) * resize_scale + img_resized.cols / 2;
    #     #eyesC2[1] = eyesC[1] * resize_scale;#(eyesC.y - img_rot.rows / 2) * resize_scale + img_resized.rows / 2;
    #
    #     # Crop
    #     resized_rows, resized_cols, resized_ch = img_resized.shape
    #     crop_y = max(0, int(eyesC2[1]) - ec_y);
    #     crop_y_end = min(resized_rows - 1, crop_y + img_size[1]);
    #     crop_x = max(0, int(eyesC2[0]) - img_size[0] / 2);
    #     crop_x_end = min(resized_cols - 1, crop_x + img_size[0]);
    #
    #     #const cv::Mat fromROI = cv::Mat(img_resized, cv::Range(crop_y, crop_y_end), cv::Range(crop_x, crop_x_end));
    #     fromROI = img_resized[crop_y:crop_y_end, crop_x:crop_x_end]
    #
    #     #const cv::Mat cropped_img(img_size, img_resized.type(),cv::Scalar(0));
    #     #cropped_img = np.zeros((img_size[0], img_size[1], resized_ch), dtype = "uint8")
    #
    #     #cv::Mat toROI = cv::Mat(cropped_img, cv::Range(0, crop_y_end - crop_y), cv::Range(0, crop_x_end -  crop_x));
    #     #toROI = cropped_img[0:crop_y_end - crop_y,0:crop_x_end -  crop_x]
    #
    #     #fromROI.copyTo(toROI);
    #     #toROI = fromROI.copy()
    #     cropped_img = fromROI.copy()
    #     res_rows, res_cols, res_ch = cropped_img.shape
    #     if res_rows < img_size[1] or res_cols < img_size[0]:
    #         cv2.imwrite('res.jpg', cropped_img)
    #         print "Bad col and row"
    #         return None
    #     return cropped_img;
    def order_points(self, pts):
        # initialzie a list of coordinates that will be ordered
        # such that the first entry in the list is the top-left,
        # the second entry is the top-right, the third is the
        # bottom-right, and the fourth is the bottom-left
        rect = np.zeros((4, 2), dtype="float32")

        # the top-left point will have the smallest sum, whereas
        # the bottom-right point will have the largest sum
        s = pts.sum(axis=1)
        rect[0] = pts[np.argmin(s)]
        rect[2] = pts[np.argmax(s)]

        # now, compute the difference between the points, the
        # top-right point will have the smallest difference,
        # whereas the bottom-left will have the largest difference
        diff = np.diff(pts, axis=1)
        rect[1] = pts[np.argmin(diff)]
        rect[3] = pts[np.argmax(diff)]

        # return the ordered coordinates
        return rect

    def four_point_transform(self, image, pts):
        # obtain a consistent order of the points and unpack them
        # individually
        rect = self.order_points(pts)
        (tl, tr, br, bl) = rect

        # compute the width of the new image, which will be the
        # maximum distance between bottom-right and bottom-left
        # x-coordiates or the top-right and top-left x-coordinates
        widthA = np.sqrt(((br[0] - bl[0]) ** 2) + ((br[1] - bl[1]) ** 2))
        widthB = np.sqrt(((tr[0] - tl[0]) ** 2) + ((tr[1] - tl[1]) ** 2))
        maxWidth = max(int(widthA), int(widthB))

        # compute the height of the new image, which will be the
        # maximum distance between the top-right and bottom-right
        # y-coordinates or the top-left and bottom-left y-coordinates
        heightA = np.sqrt(((tr[0] - br[0]) ** 2) + ((tr[1] - br[1]) ** 2))
        heightB = np.sqrt(((tl[0] - bl[0]) ** 2) + ((tl[1] - bl[1]) ** 2))
        maxHeight = max(int(heightA), int(heightB))

        # now that we have the dimensions of the new image, construct
        # the set of destination points to obtain a "birds eye view",
        # (i.e. top-down view) of the image, again specifying points
        # in the top-left, top-right, bottom-right, and bottom-left
        # order
        dst = np.array([
            [0, 0],
            [maxWidth - 1, 0],
            [maxWidth - 1, maxHeight - 1],
            [0, maxHeight - 1]], dtype="float32")

        # compute the perspective transform matrix and then apply it
        M = cv2.getPerspectiveTransform(rect, dst)
        warped = cv2.warpPerspective(image, M, (maxWidth, maxHeight), borderMode=cv2.BORDER_CONSTANT,
                                     borderValue=(255, 255, 255))

        # return the warped image
        return warped

    def cropImg(self, img, bb, landmarks):
        #(bb.left(), bb.bottom()), (bb.right(), bb.top())
        width = bb.width()
        print bb.left()
        print bb.bottom()
        x0 = int(bb.left() - 0.4 * width)
        y0 = int(bb.top() - 0.4 * width)
        w = int(1.8 * width)
        h = int(1.8 * width)

        print bb
        print str(x0) + " " + str(y0) + " " + str(w) + " " + str(h)

        pts = np.float32([[x0, y0], [x0 + w, y0], [x0 + w, y0 + h], [x0, y0 + h]])
        cropped = self.four_point_transform(img, pts)
        bb1 = dlib.rectangle(left=bb.left() - x0, top=bb.top() - y0, right=bb.right() - x0, bottom=bb.bottom() - y0)
        landmarks1 = []
        for landmark in landmarks:
            landmarks1.append((landmark[0] - x0, landmark[1] - y0))
        return cropped, bb1, landmarks1

    @staticmethod
    def gey_eye_distance(landmarks):
        r_corner = landmarks[36]
        l_corner = landmarks[45]
        dist = math.sqrt((r_corner[0] - l_corner[0]) ** 2 + (r_corner[1] - l_corner[1]) ** 2)
        return dist
