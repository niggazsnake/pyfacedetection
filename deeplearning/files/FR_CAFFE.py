import sys
sys.path.append('/home/ubuntu/caffe-face/python')
import caffe
import cv2
import numpy as np
from sklearn.metrics.pairwise import cosine_similarity
import matplotlib.pyplot as plt
from scipy import spatial
import math
from itertools import izip


class FR_CAFFE:
    def __init__(self, feature_extraction_proto, pretrained_binary_proto, blob_name, useGPU, deviceID,
                 image_dims=None, mean=None, input_scale=None, raw_scale=None, channel_swap=None):
        caffe.set_device(deviceID)
        if (useGPU):
            caffe.set_mode_gpu()
        else:
            caffe.set_mode_cpu()


            # self.net = caffe.Classifier(feature_extraction_proto,pretrained_binary_proto,
        # mean=None,
        # channel_swap=(2, 1, 0),
        #		raw_scale=255,
        #		image_dims=(112, 96))

        self.net = caffe.Net(feature_extraction_proto, pretrained_binary_proto, caffe.TEST)

        # get the number of channels
        self.channels = self.net.blobs['data'].data[0].shape[0]


        #caffe.Net.__init__(self, feature_extraction_proto, pretrained_binary_proto, caffe.TEST)
        #self.net.set_phase_test()
        self.features_blob_name = blob_name;

        # in_ = self.inputs[0]

    # self.transformer = caffe.io.Transformer(
    # {in_: self.blobs[in_].data.shape})
    # 		self.transformer.set_transpose(in_, (2, 0, 1))
    # 		if mean is not None:
    # 			self.transformer.set_mean(in_, mean)
    # 		if input_scale is not None:
    # 			self.transformer.set_input_scale(in_, input_scale)
    # 		if raw_scale is not None:
    # 			self.transformer.set_raw_scale(in_, raw_scale)
    # 		if channel_swap is not None:
    # 			self.transformer.set_channel_swap(in_, channel_swap)
    #
    # 		self.crop_dims = np.array(self.blobs[in_].data.shape[2:])
    # 		if not image_dims:
    # 			image_dims = self.crop_dims
    # 		self.image_dims = image_dims


    def getTemplate(self, input_img):

        if (input_img is None):
            return None

        img = input_img.astype(np.float32)
        #img = img/255.0
        img = (img - 127.5) / 128

        # convert the image for CNN
        if self.channels == 3:
            batch = np.zeros((3, img.shape[0], img.shape[1]))
            if len(img.shape) == 3:  # color image
                batch[0, :, :] = img[:, :, 0]
                batch[1, :, :] = img[:, :, 1]
                batch[2, :, :] = img[:, :, 2]
            else:  # grayscale image
                batch[0, :, :] = img[:, :]
                batch[1, :, :] = img[:, :]
                batch[2, :, :] = img[:, :]
        else:
            batch = np.zeros((1, img.shape[0], img.shape[1]))
            if len(img.shape) == 3:  # color image
                batch[0, :, :] = (img[:, :, 0] + img[:, :, 1] + img[:, :, 2]) / 3
            else:  # grayscale image
                batch[0, :, :] = img[:, :]

        self.net.blobs['data'].data[0] = batch
        self.net.forward(end=self.features_blob_name)
        output = np.copy(self.net.blobs[self.features_blob_name].data[0])
        N = len(output)

        #plt.bar(xrange(N), output)
        #plt.xlim([0, N])
        #plt.show()

        #out = self.net.forward()
        #self.features_blob_name = 'fc5'
        #templ = out[self.features_blob_name]
        #templ = self.net.predict([input_image], False)
        #cv2.imwrite("testr.jpg", input_image)
        #print 'templ'
        #self.net.Forward([input_image], output_blobs)
        #templ = output
        #print templ

        #for t in output:
        #	print t
        #print 'templ_end'
        #print 'o'
        #print output
        #print 'o'
        return output

    def dot_product(self, v1, v2):
        return sum(map(lambda x: x[0] * x[1], izip(v1, v2)))

    def cosine_measure(self, v1, v2):
        prod = self.dot_product(v1, v2)
        len1 = math.sqrt(self.dot_product(v1, v1))
        len2 = math.sqrt(self.dot_product(v2, v2))
        return prod / (len1 * len2)

    def cosinDistance(self, templ1, templ2):
        #templ1.reshape(-1,1)
        #templ2.reshape(-1,1)
        #return cosine_similarity([templ1], [templ2])#[0][0]
        #return spatial.distance.cosine(templ1, templ2)
        #return cosine_similarity(templ1, templ2)
        #print 't1'
        #print templ1
        #print 't2'
        #print templ2
        if (templ1 is None or templ2 is None):
            return None
        return self.cosine_measure(templ1, templ2)

    def fitFacePoints(self, img, landmarks, img_size):
        ec_mc_y = 41  # FOR TRAINING & TEST
        ec_y = 50  #40; # FOR TEST
        #ec_y = 48; # FOR TRAINING
        #cv::Size img_size(144, 144); # FOR TRAINING
        #img_size = (96, 112); #// FOR TEST
        rEyeC = ((landmarks[36][0] + landmarks[39][0]) / 2.0, (landmarks[36][1] + landmarks[39][1]) / 2.0)
        lEyeC = ((landmarks[42][0] + landmarks[45][0]) / 2.0, (landmarks[42][1] + landmarks[45][1]) / 2.0)
        rMouth = (landmarks[48][0], landmarks[48][1])
        lMouth = (landmarks[54][0], landmarks[54][1])
        noseTip = (landmarks[30][0], landmarks[30][1])

        # Rotate according eyes centers
        ang_tan = (lEyeC[1] - rEyeC[1]) / (lEyeC[0] - rEyeC[0])
        ang = math.atan(ang_tan) / math.pi * 180.0
        rows, cols, ch = img.shape
        r = cv2.getRotationMatrix2D((cols, rows / 2), ang, 1.0)
        img_rot = cv2.warpAffine(img, r, (cols, rows))
        print r
        print r[0, 2]
        # Rotate eyes center point
        eyesCBase = ((rEyeC[0] + lEyeC[0]) / 2.0, (rEyeC[1] + lEyeC[1]) / 2.0)
        eyesC = (r[0, 0] * eyesCBase[0] + r[0, 1] * eyesCBase[1] + r[0, 2],
                 r[1, 0] * eyesCBase[0] + r[1, 1] * eyesCBase[1] + r[1, 2])

        print eyesC
        # Rotate mouth center point
        mouthCBase = ((rMouth[0] + lMouth[0]) / 2.0, (rMouth[1] + lMouth[1]) / 2.0)
        mouthC = (r[0, 0] * mouthCBase[0] + r[0, 1] * mouthCBase[1] + r[0, 2],
                  r[1, 0] * mouthCBase[0] + r[1, 1] * mouthCBase[1] + r[1, 2])

        # Resize to given ec_mc_y
        resize_scale = float(ec_mc_y) / (mouthC[1] - eyesC[1])
        rot_rows, rot_cols, rot_ch = img_rot.shape
        img_resized = cv2.resize(img_rot, (int(rot_cols * resize_scale), int(rot_rows * resize_scale)))
        eyesC2 = (eyesC[0] * resize_scale, eyesC[1] * resize_scale)
        #cv2.imwrite('resized.jpg', img_resized)
        #eyesC2[0] = eyesC[0] * resize_scale;#(eyesC.x - img_rot.cols / 2) * resize_scale + img_resized.cols / 2;
        #eyesC2[1] = eyesC[1] * resize_scale;#(eyesC.y - img_rot.rows / 2) * resize_scale + img_resized.rows / 2;
        # Crop
        resized_rows, resized_cols, resized_ch = img_resized.shape
        crop_y = max(0, int(eyesC2[1]) - ec_y)

        crop_y_end = min(resized_rows - 1, crop_y + img_size[1])
        crop_x = max(0, int(eyesC2[0]) - img_size[0] / 2)
        crop_x_end = min(resized_cols - 1, crop_x + img_size[0])

        #const cv::Mat fromROI = cv::Mat(img_resized, cv::Range(crop_y, crop_y_end), cv::Range(crop_x, crop_x_end));
        fromROI = img_resized[crop_y:crop_y_end, crop_x:crop_x_end]

        #const cv::Mat cropped_img(img_size, img_resized.type(),cv::Scalar(0));
        #cropped_img = np.zeros((img_size[0], img_size[1], resized_ch), dtype = "uint8")

        #cv::Mat toROI = cv::Mat(cropped_img, cv::Range(0, crop_y_end - crop_y), cv::Range(0, crop_x_end -  crop_x));
        #toROI = cropped_img[0:crop_y_end - crop_y,0:crop_x_end -  crop_x]

        #fromROI.copyTo(toROI);
        #toROI = fromROI.copy()
        cropped_img = fromROI.copy()
        res_rows, res_cols, res_ch = cropped_img.shape
        if res_rows < img_size[1] or res_cols < img_size[0]:
            cv2.imwrite('res.jpg', cropped_img)
            print "Bad col and row"
            return None
        return cropped_img