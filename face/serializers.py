# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.core.files import File
from rest_framework import serializers
from face.models import Face
from deeplearning.helpers import get_template_from_image2
from . import FaceObject


class FaceSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Face
        fields = ('image', 'name', )


class IdentifyFaceSerializer(serializers.Serializer):
    image = serializers.FileField()

    def create(self, validated_data):
        return FaceObject(**validated_data)

    def update(self, instance, validated_data):
        for field, value in validated_data.items():
            setattr(instance, field, value)
        return instance


class FaceRegisterSerializer(serializers.ModelSerializer):
    class Meta:
        model = Face
        fields = ('image', 'name', 'username', 'password')

    def create(self, validated_data):
        image = validated_data.pop('image')
        name = validated_data.pop('name')
        username = validated_data.pop('username')
        password = validated_data.pop('password')
        template = get_template_from_image2(image)
        if not isinstance(template, dict):
            raise serializers.ValidationError(template)
        else:
            template = template.get('template', None)
            dist = template.get('distance', 0)
            if template is not None and template.any():
                #if not Face.is_token_valid_cls(username, password):
                #    raise serializers.ValidationError('User not registered on the NetHealth')

                f = File(open('cropp1.jpg'))
                face = Face.objects.create(template=template, image='img.jpg', name=name, username=username,
                                           password=password, eye_distance=dist)
                face.image.save('img.jpg', f)
                return face

        raise serializers.ValidationError('No template found')