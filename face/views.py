# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from rest_framework import viewsets
from rest_framework.response import Response
from deeplearning.helpers import search_face

from face.models import Face
from face.serializers import IdentifyFaceSerializer, FaceRegisterSerializer


class FaceViewSet(viewsets.ModelViewSet):
    queryset = Face.objects.all()
    serializer_class = FaceRegisterSerializer


class IdentifyFace(viewsets.ViewSet):
    serializer_class = IdentifyFaceSerializer

    def list(self, request):
        serializer = IdentifyFaceSerializer()
        return Response(serializer.data)

    def post(self, request, *args, **kwargs):
        serializer = IdentifyFaceSerializer(data=request.data)
        serializer.is_valid(raise_exception=False)
        face = serializer.save()
        results = search_face(face.image)
        if results and len(results) > 1:
            face_person, image_path = results
            # face_person = None
            if face_person:
                # token = face_person.is_token_valid()
                #                if token:
                token = ''
                return Response({'person': face_person.name, 'token': token, 'image': image_path})
                #	        else:
                #		    Response({'error': 'Invalid token'})

        return Response({'person': 'Неизвестно'})
