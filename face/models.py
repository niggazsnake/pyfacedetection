# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import requests
import json
from requests.auth import HTTPBasicAuth
from django.db import models
from django_numpy.fields import NumpyArrayField


class Face(models.Model):
    template = NumpyArrayField(verbose_name='Шаблон лица', base_field=models.FloatField(max_length=20))
    image = models.ImageField(verbose_name='Фото лица', upload_to='faces/%Y/%m/%d/')
    name = models.CharField(verbose_name='Имя', max_length=255, blank=True, null=True)
    username = models.CharField(verbose_name='Логин', max_length=255, blank=True, null=True)
    password = models.CharField(verbose_name='Пароль', max_length=255, blank=True, null=True)
    eye_distance = models.PositiveIntegerField(verbose_name='Расстояние между глаз', default=0)

    class Meta:
        verbose_name = 'Лицо'
        verbose_name_plural = 'Лица'

    @classmethod
    def is_token_valid_cls(cls, username, password):
        return cls.check_token(username, password)

    def is_token_valid(self):
        return self.check_token(self.username, self.password)

    @staticmethod
    def check_token(username, password):
        url = 'http://nethealth.ru/api/v2/user/token'
        response = requests.get(url, auth=HTTPBasicAuth(username, password))
        if response and response.status_code == 200:
            text = json.loads(response.text)
            data = text.get('data', None)
            if data:
                token = data.get('token', None)
                if token:
                    return token
        return None