# -*- coding: utf-8 -*-
# Generated by Django 1.10.6 on 2017-03-03 12:23
from __future__ import unicode_literals

from django.db import migrations, models
import django_numpy.fields


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Face',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('template', django_numpy.fields.NumpyArrayField(base_field=models.FloatField(max_length=20), size=None, verbose_name='\u0428\u0430\u0431\u043b\u043e\u043d \u043b\u0438\u0446\u0430')),
                ('image', models.ImageField(upload_to=b'', verbose_name='\u0424\u043e\u0442\u043e \u043b\u0438\u0446\u0430')),
            ],
            options={
                'verbose_name': '\u041b\u0438\u0446\u043e',
                'verbose_name_plural': '\u041b\u0438\u0446\u0430',
            },
        ),
    ]
